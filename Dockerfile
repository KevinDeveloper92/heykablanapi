# Start with a base image containing Java runtime
# FROM openjdk:11.0.3-jdk-stretch | adoptopenjdk/openjdk11:alpine-slim
FROM adoptopenjdk/openjdk11:alpine-slim

# Add Maintainer Info
LABEL maintainer="Kevin Abreu <kjaakevin@gmail.com>"

# Add a volume pointing to /tmp
VOLUME /tmp

# Make port 8080 available to the world outside this container
EXPOSE 8080

# The application's jar file
ARG JAR_FILE=target/heykablanapi-0.0.1-SNAPSHOT.jar

# Add the application's jar to the container
ADD ${JAR_FILE} heykablanapi.jar

# Run the jar file HelloWorld
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/heykablanapi.jar"]
#ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-javaagent:apm-agent.jar","-Delastic.apm.service_name=woodstock-xxxxxx","-Delastic.apm.server_urls=https://sistemas.forus.cl/apm","-Delastic.apm.secret_token=o3I4KUWc9GFfp7x9UlDU4KUWc9GFfp7x9UlDU6GqaUmenGp4dv","-Delastic.apm.application_packages=cl.forus","-jar","/HelloWorld.jar"]
