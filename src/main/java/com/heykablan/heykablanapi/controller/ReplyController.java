package com.heykablan.heykablanapi.controller;

import com.heykablan.heykablanapi.payload.ReplyPayload;
import com.heykablan.heykablanapi.service.IReplyService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api/replies")
public class ReplyController {

    private final IReplyService replyService;

    public ReplyController(
            IReplyService replyService
    ) {
        this.replyService = replyService;
    }

    @GetMapping("/comment/{idComment}")
    public Map<String, Object> getReplies(
            @PathVariable(value = "idComment") Integer id,
            @RequestParam(defaultValue = "0") Integer pageNo,
            @RequestParam(defaultValue = "5") Integer pageSize
    ) {
        return this.replyService.getReplies(id, pageNo, pageSize);
    }

    @PostMapping("/{idComment}")
    public ResponseEntity<Object> saveReply(
            @PathVariable(value = "idComment") Integer id,
            @RequestBody ReplyPayload reply
    ) {
        return new ResponseEntity<>(this.replyService.saveReply(id, reply), HttpStatus.CREATED);
    }
}
