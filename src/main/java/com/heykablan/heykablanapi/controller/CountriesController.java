package com.heykablan.heykablanapi.controller;

import com.heykablan.heykablanapi.entity.Countries;
import com.heykablan.heykablanapi.service.ICountriesService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/countries")
public class CountriesController {

    private final ICountriesService countriesService;

    public CountriesController(ICountriesService countriesService) {
        this.countriesService = countriesService;
    }

    @GetMapping("/")
    public List<Countries> getCountries() {
        return countriesService.getCountries();
    }
}
