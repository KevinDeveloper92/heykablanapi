package com.heykablan.heykablanapi.controller;

import com.heykablan.heykablanapi.entity.PostUpvotes;
import com.heykablan.heykablanapi.entity.Posts;
import com.heykablan.heykablanapi.payload.PostIdsPayload;
import com.heykablan.heykablanapi.service.IPostService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/posts")
public class PostController {

    private final IPostService postService;

    public PostController(IPostService postService) {
        this.postService = postService;
    }

    @GetMapping("/section/{id_category}/posts")
    public List<Posts> getAllPosts(
            @PathVariable(name = "id_category") Integer idCategory,
            @RequestParam(defaultValue = "0") Integer pageNo,
            @RequestParam(defaultValue = "10") Integer pageSize
    ) {
        return postService.getAllPosts(idCategory, pageNo, pageSize);
    }

    @GetMapping("/section/{id_category}/ids")
    public List<PostIdsPayload> getAllPostIdsByCategory(
            @PathVariable(name = "id_category") Integer idCategory
    ) {
        return postService.getAllPostIdsByCategory(idCategory);
    }

    @GetMapping("/section/post/{idPost}/detail")
    public Posts getPostDetail(
            @PathVariable("idPost") Integer id
    ) {
        return postService.getPostDetail(id);
    }

    @GetMapping("/section/random")
    public List<Posts> getRandomPosts() {
        return postService.getRandomPosts();
    }

    @GetMapping("/section/all/searchBy")
    public List<Posts> searchPostBy(
            @RequestParam("title") String title
    ) {
        return this.postService.searchPostBy(title);
    }

    @PostMapping(
            path = "/upload",
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Posts uploadPost(
            @RequestParam("file") MultipartFile file,
            @RequestParam("title") String title,
            @RequestParam("id_category") Integer idCategory
    ) {
        return postService.upload(file, title, idCategory);
    }

    @PostMapping("/upvote/{id_post}")
    public PostUpvotes upvotePost(
            @PathVariable("id_post") Integer idPost
    ) {
        return postService.upvotePost(idPost);
    }
}
