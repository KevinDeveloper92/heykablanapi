package com.heykablan.heykablanapi.controller;

import com.heykablan.heykablanapi.entity.Categories;
import com.heykablan.heykablanapi.repository.CategoriesRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/categories")
public class CategoriesController {

    private final CategoriesRepository categoriesRepository;

    public CategoriesController(CategoriesRepository categoriesRepository) {
        this.categoriesRepository = categoriesRepository;
    }

    @GetMapping("/")
    public List<Categories> getAll() {
        return categoriesRepository.findAll();
    }
}
