package com.heykablan.heykablanapi.controller;

import com.heykablan.heykablanapi.entity.Users;
import com.heykablan.heykablanapi.payload.UserInfoPayload;
import com.heykablan.heykablanapi.service.IUserService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/user")
public class UserController {

    private final IUserService userService;

    public UserController(IUserService userService) {
        this.userService = userService;
    }

    @GetMapping("/{idUser}")
    public Users getUserDetail(
            @PathVariable(value = "idUser") Integer id
    ) {
        return userService.getUserDetail(id);
    }

    @GetMapping("/settings")
    public Users getUserInfo() {
        return userService.getUserInfo();
    }

    @PutMapping("/update/{idUser}")
    public Users updateUserInfo(
            @PathVariable(value = "idUser") Integer id,
            @RequestBody UserInfoPayload user
    ) {
        return userService.updateUserInfo(id, user);
    }

    @PutMapping(path = "/update/{idUser}/photo", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Users updateUserPhoto(
            @PathVariable(value = "idUser") Integer id,
            @RequestParam("file") MultipartFile file
    ) {
        return userService.updateUserPhoto(id, file);
    }
}
