package com.heykablan.heykablanapi.controller;

import com.heykablan.heykablanapi.entity.PostComments;
import com.heykablan.heykablanapi.entity.PostCommentsUpvotes;
import com.heykablan.heykablanapi.payload.CommentPayload;
import com.heykablan.heykablanapi.payload.CommentUpVotePayload;
import com.heykablan.heykablanapi.service.IPostCommentService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/api/comments")
@RestController
public class PostCommentsController {

    private final IPostCommentService postCommentService;

    public PostCommentsController(IPostCommentService postCommentService) {
        this.postCommentService = postCommentService;
    }

    @GetMapping("/post/{idPost}/comments")
    public List<PostComments> getCommentsByPost(
            @PathVariable(name = "idPost") Integer idPost,
            @RequestParam(defaultValue = "0") Integer pageNo,
            @RequestParam(defaultValue = "10") Integer pageSize
    ) {
        return postCommentService.getCommentsByPost(idPost, pageNo, pageSize);
    }

    @PostMapping("/save/comment")
    public PostComments commentPost(
            @RequestBody CommentPayload payload
    ) {
        return postCommentService.commentPost(payload.getComment(), payload.getIdPost());
    }

    @PostMapping("/save/comment/upvote")
    public PostCommentsUpvotes saveCommentUpVote(
            @RequestBody CommentUpVotePayload payload
    ) {
        return postCommentService.saveCommentUpVote(payload.getIdComment(), payload.getUpvote());
    }
}
