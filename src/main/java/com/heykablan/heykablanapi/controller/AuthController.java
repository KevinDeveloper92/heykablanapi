package com.heykablan.heykablanapi.controller;

import com.heykablan.heykablanapi.entity.Users;
import com.heykablan.heykablanapi.model.JWTModel;
import com.heykablan.heykablanapi.payload.LoginRequest;
import com.heykablan.heykablanapi.payload.RegisterRequest;
import com.heykablan.heykablanapi.service.UserServiceImpl;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/auth")
public class AuthController {

    UserServiceImpl userAuthService;

    public AuthController(UserServiceImpl userService) {
        this.userAuthService = userService;
    }

    @PostMapping("/login")
    public JWTModel login(@RequestBody LoginRequest loginRequest) {
        return userAuthService.loginUser(loginRequest);
    }

    @PostMapping("/register")
    public Users register(@RequestBody RegisterRequest registerRequest) {
        return userAuthService.saveUser(registerRequest);
    }
}
