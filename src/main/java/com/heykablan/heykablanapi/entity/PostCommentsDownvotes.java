package com.heykablan.heykablanapi.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@Table(name = "post_comments_downvotes")
public class PostCommentsDownvotes {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private int downvote;

    @Column(name = "created_at")
    private Date createdAt;

    @ManyToOne
    @JoinColumn(name = "id_comment")
    private PostComments comment;

    @ManyToOne
    @JoinColumn(name = "id_user")
    private Users user;
}
