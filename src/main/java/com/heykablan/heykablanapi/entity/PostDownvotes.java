package com.heykablan.heykablanapi.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@Table(name = "post_downvotes")
public class PostDownvotes {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private int downvote;

    @Column(name = "created_at")
    private Date createdAt;

    @ManyToOne
    @JoinColumn(name = "id_post")
    private Posts post;

    @ManyToOne
    @JoinColumn(name = "id_user")
    private Users user;
}
