package com.heykablan.heykablanapi.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@Table(name = "post_comments_upvotes")
public class PostCommentsUpvotes {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private int upvote;

    @Column(name = "created_at")
    private Date createdAt;

    @ManyToOne
    @JoinColumn(name = "id_comment")
    @JsonBackReference
    private PostComments comment;

    @ManyToOne
    @JoinColumn(name = "id_user")
    private Users user;
}
