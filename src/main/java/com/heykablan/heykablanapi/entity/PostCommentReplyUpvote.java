package com.heykablan.heykablanapi.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "post_comment_reply_upvote")
public class PostCommentReplyUpvote {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @Column
    private int upvote;

    @Column
    private Date createdAt;

    @ManyToOne
    @JoinColumn(name = "id_reply")
    @JsonBackReference
    private PostCommentReply reply;

    @ManyToOne
    @JoinColumn(name = "id_user")
    private Users user;
}
