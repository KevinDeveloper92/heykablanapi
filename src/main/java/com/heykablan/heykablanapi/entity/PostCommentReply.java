package com.heykablan.heykablanapi.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "post_comment_reply")
public class PostCommentReply {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @Column
    private String comment;

    @Column
    private Date createdAt;

    @ManyToOne
    @JoinColumn(name = "id_comment")
    @JsonBackReference
    private PostComments postComment;

    @ManyToOne
    @JoinColumn(name = "id_user")
    private Users user;

    @OneToMany(mappedBy = "reply", fetch = FetchType.EAGER)
    @JsonManagedReference
    @Fetch(value = FetchMode.SUBSELECT)
    private Collection<PostCommentReplyUpvote> replyUpVotes;
}
