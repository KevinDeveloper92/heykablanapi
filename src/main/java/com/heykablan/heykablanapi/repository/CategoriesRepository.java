package com.heykablan.heykablanapi.repository;

import com.heykablan.heykablanapi.entity.Categories;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoriesRepository extends JpaRepository<Categories, Integer> {

    @Query(value = "SELECT * FROM categories WHERE id NOT IN (1, 2, 3)",nativeQuery = true)
    List<Categories> findAll();
}
