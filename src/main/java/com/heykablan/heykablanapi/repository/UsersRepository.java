package com.heykablan.heykablanapi.repository;

import com.heykablan.heykablanapi.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UsersRepository extends JpaRepository<Users, Integer> {

    Optional<Users> findByEmailAndPassword(String email, String password);

    Optional<Users> findByEmail(String email);

    Optional<Users> findByEmailAndUsername(String email, String username);

    Optional<Users> findByIdOrUsername(Integer id, String username);

    Users findByUsername(String username);

    Optional<Users> findByEmailOrUsername(String email, String username);
}
