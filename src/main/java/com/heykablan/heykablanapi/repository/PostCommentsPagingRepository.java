package com.heykablan.heykablanapi.repository;

import com.heykablan.heykablanapi.entity.Categories;
import com.heykablan.heykablanapi.entity.PostComments;
import com.heykablan.heykablanapi.entity.Posts;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostCommentsPagingRepository extends PagingAndSortingRepository<PostComments, Integer> {

    Page<PostComments> findByPost(Posts post, Pageable paging);
}
