package com.heykablan.heykablanapi.repository;

import com.heykablan.heykablanapi.entity.PostCommentsUpvotes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostCommentsUpVoteRepository extends JpaRepository<PostCommentsUpvotes, Integer> {


}
