package com.heykablan.heykablanapi.repository;

import com.heykablan.heykablanapi.entity.PostComments;
import com.heykablan.heykablanapi.entity.Posts;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PostCommentsRepository extends JpaRepository<PostComments, Integer> {

    List<PostComments> findByPostOrderByCreatedAtDesc(Posts post);
}
