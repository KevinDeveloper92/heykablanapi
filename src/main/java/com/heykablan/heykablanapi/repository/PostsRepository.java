package com.heykablan.heykablanapi.repository;

import com.heykablan.heykablanapi.entity.Categories;
import com.heykablan.heykablanapi.entity.Posts;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface PostsRepository extends JpaRepository<Posts, Integer> {

    List<Posts> findAllByOrderByCreatedAtDesc();

    Optional<Posts> findByIdOrderByCommentsCreatedAtAsc(Integer idPost);

    List<Posts> findByCategory(Categories category);

    Optional<Posts> findByIdAndCategory(Integer idPost, Categories category);

    @Query(value = "SELECT * FROM posts ORDER BY rand() LIMIT 6", nativeQuery = true)
    List<Posts> findRandomPosts();

    List<Posts> findByTitleContainingIgnoreCase(String title);
}
