package com.heykablan.heykablanapi.repository;

import com.heykablan.heykablanapi.entity.PostCommentReply;
import com.heykablan.heykablanapi.entity.PostComments;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ReplyPagingRepository extends JpaRepository<PostCommentReply, Integer> {

    Page<PostCommentReply> findByPostComment(PostComments comment, Pageable paging);
}
