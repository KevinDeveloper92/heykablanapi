package com.heykablan.heykablanapi.repository;

import com.heykablan.heykablanapi.entity.PostCommentReply;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReplyRepository extends JpaRepository<PostCommentReply, Integer> {
}
