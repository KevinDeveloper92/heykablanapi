package com.heykablan.heykablanapi.repository;

import com.heykablan.heykablanapi.entity.Countries;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountriesRepository extends JpaRepository<Countries, Integer> {
}
