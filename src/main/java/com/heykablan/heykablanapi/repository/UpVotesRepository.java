package com.heykablan.heykablanapi.repository;

import com.heykablan.heykablanapi.entity.PostUpvotes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UpVotesRepository extends JpaRepository<PostUpvotes, Integer> {
}
