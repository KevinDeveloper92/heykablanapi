package com.heykablan.heykablanapi.service;

import com.heykablan.heykablanapi.entity.Users;
import com.heykablan.heykablanapi.model.JWTModel;
import com.heykablan.heykablanapi.payload.LoginRequest;
import com.heykablan.heykablanapi.payload.RegisterRequest;
import com.heykablan.heykablanapi.payload.UserInfoPayload;
import org.springframework.web.multipart.MultipartFile;

public interface IUserService {

    Users saveUser(RegisterRequest registerRequest);

    Users getUserDetail(Integer id);

    JWTModel loginUser(LoginRequest loginRequest);

    Users getUserInfo();

    Users updateUserInfo(Integer id, UserInfoPayload user);

    Users updateUserPhoto(Integer id, MultipartFile file);
}
