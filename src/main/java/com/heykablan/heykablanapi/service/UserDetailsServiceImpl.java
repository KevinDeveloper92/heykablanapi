package com.heykablan.heykablanapi.service;

import com.heykablan.heykablanapi.entity.Users;
import com.heykablan.heykablanapi.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.util.Optional;

@Component
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UsersRepository usersRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

         Optional<Users> user = usersRepository.findByEmail(s);

         if (user.isPresent()) {
             Users usr = user.get();
             return new User(usr.getEmail(), usr.getPassword(), usr.getAuthorities());
         } else {
             throw new UsernameNotFoundException(MessageFormat.format("El usuario con el correo {0} no fue encontrado", s));
         }
    }
}
