package com.heykablan.heykablanapi.service;

import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.heykablan.heykablanapi.entity.*;
import com.heykablan.heykablanapi.exception.ApiRequestException;
import com.heykablan.heykablanapi.payload.PostIdsPayload;
import com.heykablan.heykablanapi.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class PostServiceImpl implements IPostService {

    private static final Logger logger = LoggerFactory.getLogger(PostServiceImpl.class);

    @Value("${amazonProperties.endpointUrl}")
    private String endpointUrl;

    @Value("${amazonProperties.bucketName}")
    private String bucketName;

    @Value("${amazonProperties.accessKey}")
    private String accessKey;

    @Value("${amazonProperties.secretKey}")
    private String secretKey;

    @Value("${amazonProperties.region}")
    private String clientRegion;

    private final PostsRepository postsRepository;

    private final UsersRepository usersRepository;

    private final CategoriesRepository categoriesRepository;

    private final PostPagingRepository pagingRepository;

    private final UpVotesRepository upVotesRepository;

    private static final String POST_NOT_FOUND = "No se encontró el post";
    private static final String USER_NOT_FOUND = "No se encontró el usuario";
    private static final String CATEGORY_NOT_FOUND = "No se encontró la categoría";

    public PostServiceImpl(
            PostsRepository postsRepository,
            UsersRepository usersRepository,
            CategoriesRepository categoriesRepository,
            PostPagingRepository postPagingRepository,
            UpVotesRepository upVotesRepository,
            PostCommentsRepository postCommentsRepository,
            PostCommentsPagingRepository postCommentsPagingRepository
    ) {
        this.postsRepository = postsRepository;
        this.usersRepository = usersRepository;
        this.categoriesRepository = categoriesRepository;
        this.pagingRepository = postPagingRepository;
        this.upVotesRepository = upVotesRepository;
    }

    private Categories getCategory(Integer idCategory) {
        return categoriesRepository.findById(idCategory).orElse(null);
    }

    @Override
    public Posts getPostDetail(Integer id) {

        Posts postDetail = postsRepository.findByIdOrderByCommentsCreatedAtAsc(id).orElse(null);

        if (postDetail == null) {
            throw new ApiRequestException(POST_NOT_FOUND);
        }

        return postDetail;
    }

    @Override
    public Posts upload(
            MultipartFile file,
            String title,
            Integer idCategory
    ) {
        try {
            String keyName = new Date().getTime() + "-" + Objects.requireNonNull(file.getOriginalFilename()).replace(" ", "_");

            BasicAWSCredentials credentials = new BasicAWSCredentials(this.accessKey, this.secretKey);

            AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                    .withRegion(this.clientRegion)
                    .withCredentials(new AWSStaticCredentialsProvider(credentials))
                    .build();

            s3Client.putObject(new PutObjectRequest(this.bucketName, keyName, file.getInputStream(), new ObjectMetadata())
                    .withCannedAcl(CannedAccessControlList.PublicRead));

            String awsEndpoint = this.endpointUrl + keyName;

            String email = SecurityContextHolder.getContext().getAuthentication().getName();

            Optional<Users> user = usersRepository.findByEmail(email);

            Optional<Categories> category = categoriesRepository.findById(idCategory);

            Posts post = new Posts();
            if (category.isPresent() && user.isPresent()) {
                Categories categoryObject = category.get();
                Users userObject = user.get();
                post.setTitle(title);
                post.setImage(awsEndpoint);
                post.setCreatedAt(new Date());
                post.setUser(userObject);
                post.setCategory(categoryObject);
            } else {
                throw new ApiRequestException("No se consiguió este usuario o esta categoría. Verifique e intente nuevamente");
            }

            return postsRepository.save(post);
        } catch (SdkClientException | IOException e) {
            logger.warn(e.getLocalizedMessage());
            return null;
        }
    }

    @Override
    public List<Posts> getAllPosts(Integer idCategory, Integer pageNo, Integer pageSize) {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by("createdAt").descending());

        Categories category = getCategory(idCategory);

        if (category == null) {
            throw new ApiRequestException(CATEGORY_NOT_FOUND);
        }

        Page<Posts> pagedResult = pagingRepository.findAllByCategory(category, paging);

        if (pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public PostUpvotes upvotePost(Integer idPost) {
        String email = SecurityContextHolder.getContext().getAuthentication().getName();

        Users user = usersRepository.findByEmail(email).orElse(null);

        Posts post = postsRepository.findById(idPost).orElse(null);

        if (post == null) {
            throw new ApiRequestException(POST_NOT_FOUND);
        }

        if (user == null) {
            throw new ApiRequestException(USER_NOT_FOUND);
        }

        PostUpvotes upvote = new PostUpvotes();
        upvote.setUpvote(1);
        upvote.setCreatedAt(new Date());
        upvote.setPost(post);
        upvote.setUser(user);
        return upVotesRepository.save(upvote);
    }

    @Override
    public List<Posts> getRandomPosts() {
        return postsRepository.findRandomPosts();
    }

    @Override
    public List<PostIdsPayload> getAllPostIdsByCategory(Integer idCategory) {
        Categories category = getCategory(idCategory);

        if (category == null) {
            throw new ApiRequestException(CATEGORY_NOT_FOUND);
        }

        List<Posts> post = postsRepository.findByCategory(category);
        return post.stream().map(o -> {
            PostIdsPayload postId = new PostIdsPayload();
            postId.setId(o.getId());
            return postId;
        }).collect(Collectors.toList());
    }

    @Override
    public List<Posts> searchPostBy(String title) {
        return this.postsRepository.findByTitleContainingIgnoreCase(title);
    }
}
