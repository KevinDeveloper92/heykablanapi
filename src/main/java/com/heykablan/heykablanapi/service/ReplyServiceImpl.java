package com.heykablan.heykablanapi.service;

import com.heykablan.heykablanapi.entity.PostCommentReply;
import com.heykablan.heykablanapi.entity.PostComments;
import com.heykablan.heykablanapi.entity.Users;
import com.heykablan.heykablanapi.exception.ApiRequestException;
import com.heykablan.heykablanapi.payload.ReplyPayload;
import com.heykablan.heykablanapi.repository.PostCommentsRepository;
import com.heykablan.heykablanapi.repository.ReplyPagingRepository;
import com.heykablan.heykablanapi.repository.ReplyRepository;
import com.heykablan.heykablanapi.repository.UsersRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ReplyServiceImpl implements IReplyService {

    private static final String COMMENT_NOT_FOUND = "No se encontró el comentario";
    private static final String USER_NOT_FOUND = "No se encontró el usuario";

    private final ReplyPagingRepository pagingRepository;

    private final PostCommentsRepository commentsRepository;

    private final UsersRepository usersRepository;

    private final ReplyRepository replyRepository;

    public ReplyServiceImpl(
            ReplyPagingRepository pagingRepository,
            PostCommentsRepository commentsRepository,
            UsersRepository usersRepository,
            ReplyRepository replyRepository
    ) {
        this.pagingRepository = pagingRepository;
        this.commentsRepository = commentsRepository;
        this.usersRepository = usersRepository;
        this.replyRepository = replyRepository;
    }

    @Override
    public Map<String, Object> getReplies(Integer id, Integer pageNo, Integer pageSize) {

        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by("createdAt").ascending());

        PostComments comment = this.commentsRepository.findById(id).orElse(null);

        if (comment == null) {
            throw new ApiRequestException(COMMENT_NOT_FOUND);
        }

        Page<PostCommentReply> replies = this.pagingRepository.findByPostComment(comment, paging);

        if (replies.hasContent()) {

            Map<String, Object> result = new HashMap<>();

            result.put("content", replies.getContent());
            result.put("currentPage", replies.getNumber());
            result.put("totalPages", replies.getTotalPages() - 1);
            result.put("totalElements", replies.getTotalElements());
            result.put("isFirst", replies.isFirst());
            result.put("isLast", replies.isLast());
            result.put("isEmpty", replies.isEmpty());
            result.put("size", replies.getSize());

            return result;
        } else {
            return new HashMap<>();
        }
    }

    @Override
    public PostCommentReply saveReply(Integer id, ReplyPayload reply) {

        String email = SecurityContextHolder.getContext().getAuthentication().getName();

        PostComments comment = this.commentsRepository.findById(id).orElse(null);

        Users user = this.usersRepository.findByEmail(email).orElse(null);

        if (user == null) {
            throw new ApiRequestException(USER_NOT_FOUND);
        }

        if (comment == null) {
            throw new ApiRequestException(COMMENT_NOT_FOUND);
        }

        PostCommentReply commentReply = new PostCommentReply();
        commentReply.setComment(reply.getReply());
        commentReply.setCreatedAt(new Date());
        commentReply.setPostComment(comment);
        commentReply.setUser(user);

        return this.replyRepository.save(commentReply);
    }
}
