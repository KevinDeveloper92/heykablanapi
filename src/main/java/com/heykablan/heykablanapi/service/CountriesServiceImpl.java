package com.heykablan.heykablanapi.service;

import com.heykablan.heykablanapi.entity.Countries;
import com.heykablan.heykablanapi.repository.CountriesRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountriesServiceImpl implements ICountriesService {

    private final CountriesRepository countriesRepository;

    public CountriesServiceImpl(
            CountriesRepository countriesRepository
    ) {
        this.countriesRepository = countriesRepository;
    }

    @Override
    public List<Countries> getCountries() {
        return countriesRepository.findAll();
    }
}
