package com.heykablan.heykablanapi.service;

import com.heykablan.heykablanapi.entity.PostUpvotes;
import com.heykablan.heykablanapi.entity.Posts;
import com.heykablan.heykablanapi.payload.PostIdsPayload;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface IPostService {

    Posts upload(MultipartFile file, String title, Integer idCategory);

    Posts getPostDetail(Integer id);

    List<Posts> getAllPosts(Integer idCategory, Integer pageNo, Integer pageSize);

    PostUpvotes upvotePost(Integer idPost);

    List<Posts> getRandomPosts();

    List<PostIdsPayload> getAllPostIdsByCategory(Integer idCategory);

    List<Posts> searchPostBy(String title);
}