package com.heykablan.heykablanapi.service;

import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.heykablan.heykablanapi.entity.Countries;
import com.heykablan.heykablanapi.entity.Users;
import com.heykablan.heykablanapi.exception.ApiRequestException;
import com.heykablan.heykablanapi.model.JWTModel;
import com.heykablan.heykablanapi.payload.LoginRequest;
import com.heykablan.heykablanapi.payload.RegisterRequest;
import com.heykablan.heykablanapi.payload.UserInfoPayload;
import com.heykablan.heykablanapi.repository.CountriesRepository;
import com.heykablan.heykablanapi.repository.UsersRepository;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements IUserService {

    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    private static final String USER_NOT_FOUND = "Usuario no encontrado";

    private static final String COUNTRY_NOT_FOUND = "País no encontrado";

    @Value("${amazonProperties.endpointUrl}")
    private String endpointUrl;

    @Value("${amazonProperties.bucketName}")
    private String bucketName;

    @Value("${amazonProperties.accessKey}")
    private String accessKey;

    @Value("${amazonProperties.region}")
    private String clientRegion;

    @Value("${amazonProperties.secretKey}")
    private String secretKeyAws;

    private final PasswordEncoder passwordEncoder;

    private final UsersRepository usersRepository;

    private final CountriesRepository countriesRepository;

    public UserServiceImpl(
            PasswordEncoder passwordEncoder,
            UsersRepository usersRepository,
            CountriesRepository countriesRepository
    ) {
        this.passwordEncoder = passwordEncoder;
        this.usersRepository = usersRepository;
        this.countriesRepository = countriesRepository;
    }

    @Value("${security.jwt.token.secret-key}")
    private String secretKey;

    @Override
    public Users saveUser(RegisterRequest registerRequest) {

        Optional<Users> match = usersRepository.findByEmailOrUsername(registerRequest.getEmail(), registerRequest.getUsername());

        if (match.isPresent()) {
            throw new ApiRequestException("Ya existe un registro con el usuario o correo");
        }

        if (!registerRequest.getPassword().equals(registerRequest.getPasswordRepeat())) {
            throw new ApiRequestException("Las contraseñas no coinciden");
        }

        Users user = new Users();
        user.setUsername(registerRequest.getUsername());
        user.setEmail(registerRequest.getEmail());
        String password = passwordEncoder.encode(registerRequest.getPassword());
        user.setPassword(password);
        user.setRoles("ROLE_USER");
        user.setPhoto("https://hey-kablan-posts.s3-sa-east-1.amazonaws.com/user-profile/user.png");
        return usersRepository.save(user);
    }

    @Override
    public JWTModel loginUser(LoginRequest loginRequest) {
        Users user = usersRepository.findByEmail(loginRequest.getEmail()).orElse(null);

        if (user != null) {
            if (!passwordEncoder.matches(loginRequest.getPassword(), user.getPassword())) {
                throw new ApiRequestException("La contraseña es incorrecta");
            }
            String token = getJWTToken(user);
            JWTModel jsonWebToken = new JWTModel();
            jsonWebToken.setTokenType("Bearer");
            jsonWebToken.setToken(token);
            return jsonWebToken;
        } else {
            throw new ApiRequestException("El correo no existe");
        }
    }

    @Override
    public Users getUserInfo() {
        String email = SecurityContextHolder.getContext().getAuthentication().getName();

        Users user = usersRepository.findByEmail(email).orElse(null);

        if (user == null) {
            throw new ApiRequestException(USER_NOT_FOUND);
        }

        return user;
    }

    @Override
    public Users updateUserInfo(Integer id, UserInfoPayload payload) {

        Users user = usersRepository.findById(id).orElse(null);

        Countries country = countriesRepository.findById(payload.getIdCountry()).orElse(null);

        if (country == null) {
            throw new ApiRequestException(COUNTRY_NOT_FOUND);
        }

        if (user != null) {
            user.setUsername(payload.getUsername());
            user.setGender(payload.getGender());
            user.setDescription(payload.getDescription());
            user.setBirthdate(payload.getBirthdate());
            user.setCountry(country);
            user.setFullName(payload.getFullName());

            return usersRepository.save(user);
        } else {
            throw new ApiRequestException(USER_NOT_FOUND);
        }
    }

    @Override
    public Users updateUserPhoto(Integer id, MultipartFile file) {
        try {

            Users user = usersRepository.findById(id).orElse(null);

            if (user == null) {
                throw new ApiRequestException(USER_NOT_FOUND);
            }

            String keyName = new Date().getTime() + "-" + Objects.requireNonNull(file.getOriginalFilename()).replace(" ", "_");

            BasicAWSCredentials credentials = new BasicAWSCredentials(this.accessKey, this.secretKeyAws);

            AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                    .withRegion(this.clientRegion)
                    .withCredentials(new AWSStaticCredentialsProvider(credentials))
                    .build();

            s3Client.putObject(new PutObjectRequest(this.bucketName + "/user-profile", keyName, file.getInputStream(), new ObjectMetadata())
                    .withCannedAcl(CannedAccessControlList.PublicRead));

            String awsEndpoint = this.endpointUrl + "user-profile/" + keyName;

            user.setPhoto(awsEndpoint);

            return usersRepository.save(user);
        } catch (SdkClientException | IOException e) {
            throw new ApiRequestException(e.getMessage());
        }
    }

    @Override
    public Users getUserDetail(Integer id) {
        Optional<Users> userDetail = usersRepository.findById(id);
        if (userDetail.isPresent()) {
            return userDetail.get();
        } else {
            throw new ApiRequestException("No se encontró el usuario");
        }
    }

    private String getJWTToken(Users user) {
        List<GrantedAuthority> grantedAuthorities = AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER");
        return Jwts
                .builder()
                .setId("softtekJWT")
                .setSubject(user.getEmail())
                .claim("authorities",
                        grantedAuthorities.stream()
                                .map(GrantedAuthority::getAuthority)
                                .collect(Collectors.toList()))
                .claim("user", user)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 900000000))
                .signWith(SignatureAlgorithm.HS512,
                        this.secretKey.getBytes()).compact();
    }
}
