package com.heykablan.heykablanapi.service;

import com.heykablan.heykablanapi.entity.Countries;

import java.util.List;

public interface ICountriesService {

    List<Countries> getCountries();
}
