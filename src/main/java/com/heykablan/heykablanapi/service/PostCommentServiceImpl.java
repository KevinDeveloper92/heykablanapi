package com.heykablan.heykablanapi.service;

import com.heykablan.heykablanapi.entity.*;
import com.heykablan.heykablanapi.exception.ApiRequestException;
import com.heykablan.heykablanapi.repository.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class PostCommentServiceImpl implements IPostCommentService {

    private static final String POST_NOT_FOUND = "No se encontró el post";
    private static final String USER_NOT_FOUND = "No se encontró el usuario";
    private static final String COMMENT_NOT_FOUND = "No se encontró el comentario";

    private final UsersRepository usersRepository;
    private final PostsRepository postsRepository;
    private final PostCommentsRepository commentsRepository;
    private final CategoriesRepository categoriesRepository;
    private final PostCommentsPagingRepository postCommentsPagingRepository;
    private final PostCommentsUpVoteRepository postCommentsUpVoteRepository;

    public PostCommentServiceImpl(
            UsersRepository usersRepository,
            PostsRepository postsRepository,
            PostCommentsRepository postCommentsRepository,
            CategoriesRepository categoriesRepository,
            PostCommentsPagingRepository postCommentsPagingRepository,
            PostCommentsUpVoteRepository postCommentsUpVoteRepository
    ) {
        this.usersRepository = usersRepository;
        this.postsRepository = postsRepository;
        this.commentsRepository = postCommentsRepository;
        this.categoriesRepository = categoriesRepository;
        this.postCommentsPagingRepository = postCommentsPagingRepository;
        this.postCommentsUpVoteRepository = postCommentsUpVoteRepository;
    }

    private Categories getCategory(Integer idCategory) {
        return categoriesRepository.findById(idCategory).orElse(null);
    }

    @Override
    public PostComments commentPost(String comment, Integer idPost) {
        String email = SecurityContextHolder.getContext().getAuthentication().getName();

        Users user = usersRepository.findByEmail(email).orElse(null);

        Posts post = postsRepository.findById(idPost).orElse(null);

        if (post == null) {
            throw new ApiRequestException(POST_NOT_FOUND);
        }

        if (user == null) {
            throw new ApiRequestException(USER_NOT_FOUND);
        }

        PostComments comments = new PostComments();
        comments.setComment(comment);
        comments.setCreatedAt(new Date());
        comments.setPost(post);
        comments.setUser(user);
        return commentsRepository.save(comments);
    }

    @Override
    public List<PostComments> getCommentsByPost(Integer idPost, Integer pageNo, Integer pageSize) {

        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by("createdAt").descending());

        Posts post = postsRepository.findById(idPost).orElse(null);

        if (post == null) {
            throw new ApiRequestException(POST_NOT_FOUND);
        }

        Page<PostComments> pagedResult = postCommentsPagingRepository.findByPost(post, paging);

        if (pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public PostCommentsUpvotes saveCommentUpVote(Integer idComment, Integer quantity) {

        PostComments comment = commentsRepository.findById(idComment).orElse(null);

        String email = SecurityContextHolder.getContext().getAuthentication().getName();

        Users user = usersRepository.findByEmail(email).orElse(null);

        if (comment == null) {
            throw new ApiRequestException(COMMENT_NOT_FOUND);
        }

        if (user == null) {
            throw new ApiRequestException(USER_NOT_FOUND);
        }

        PostCommentsUpvotes upvote = new PostCommentsUpvotes();

        upvote.setUpvote(quantity);
        upvote.setUser(user);
        upvote.setComment(comment);
        upvote.setCreatedAt(new Date());

        return postCommentsUpVoteRepository.save(upvote);
    }
}
