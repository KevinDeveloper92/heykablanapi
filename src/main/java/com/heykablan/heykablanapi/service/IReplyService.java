package com.heykablan.heykablanapi.service;

import com.heykablan.heykablanapi.entity.PostCommentReply;
import com.heykablan.heykablanapi.payload.ReplyPayload;

import java.util.List;
import java.util.Map;

public interface IReplyService {

    Map<String, Object> getReplies(Integer id, Integer pageNo, Integer pageSize);

    PostCommentReply saveReply(Integer id, ReplyPayload reply);
}
