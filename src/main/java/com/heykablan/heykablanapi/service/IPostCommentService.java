package com.heykablan.heykablanapi.service;

import com.heykablan.heykablanapi.entity.PostComments;
import com.heykablan.heykablanapi.entity.PostCommentsUpvotes;

import java.util.List;

public interface IPostCommentService {

    PostComments commentPost(String comment, Integer idPost);

    List<PostComments> getCommentsByPost(Integer idPost, Integer pageNo, Integer pageSize);

    PostCommentsUpvotes saveCommentUpVote(Integer idComment, Integer upvote);
}
