package com.heykablan.heykablanapi.payload;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@Data
public class UserInfoPayload {

    @JsonProperty
    private int id;

    @JsonProperty
    private String username;

    @JsonProperty
    private String email;

    @JsonProperty(value = "full_name")
    private String fullName;

    @JsonProperty
    private Date birthdate;

    @JsonProperty
    private String description;

    @JsonProperty
    private String gender;

    @JsonProperty(value = "id_country")
    private int idCountry;

    @JsonProperty
    private String photo;
}
