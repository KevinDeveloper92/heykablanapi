package com.heykablan.heykablanapi.payload;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.Column;
import java.io.File;

@Data
public class UploadRequest {
    @Column
    private String url;
}
