package com.heykablan.heykablanapi.payload;

import lombok.Data;

@Data
public class PostIdsPayload {

    private int id;
}
