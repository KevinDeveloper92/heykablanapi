package com.heykablan.heykablanapi.payload;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ReplyPayload {

    @JsonProperty(value = "reply")
    private String reply;
}
