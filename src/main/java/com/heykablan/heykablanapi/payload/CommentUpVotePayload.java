package com.heykablan.heykablanapi.payload;

import lombok.Data;

@Data
public class CommentUpVotePayload {

    private int upvote;

    private int idComment;
}
