package com.heykablan.heykablanapi.payload;

import lombok.Data;

@Data
public class CommentPayload {

    private int idPost;

    private String comment;
}
