package com.heykablan.heykablanapi.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.io.Serializable;

@Data
@NoArgsConstructor
public class JWTModel implements Serializable {

    @Column
    private String tokenType;

    @Column
    private String token;
}
